import { createApp } from 'vue'
import App from './App.vue'

import CoreuiVue from '@coreui/vue'
import CIcon from '@coreui/icons-vue'
import store from "@/store"

const app =createApp(App)
app.use(store);
app.use(CoreuiVue);
app.component('CIcon', CIcon)

app.mount('#app')