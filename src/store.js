import { createStore } from "vuex";


function isObject(a) {
    return !!a && a.constructor === Object;
}

function isEmptyObjectCheck(obj) {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
}

function storeLocalStorage(key, payload) {
    localStorage.setItem(key, JSON.stringify(payload))
}

export default createStore({
    state: {
        userActivity: JSON.parse(localStorage.getItem("userActivity")) || []
    },
    getters: {
        userActivity: (state) => state.userActivity.map((v, index) => ({
            ...v,
            index
        })) || []
    },
    actions: {
        addNewActivity: ({ commit, getters, }, payload = {}) => {
            let { userActivity } = getters;
            // checking payload is object and not an empty object
            if (isObject(payload) && !isEmptyObjectCheck(payload)) {
                userActivity = [...userActivity, payload];
                commit("SET_USER_ACTIVITY", userActivity)
            }
            return;
        },
        updateActivity: ({ commit, getters }, { index, payload }) => {
            let { userActivity } = getters
            //check index available in stored user activity
            if (typeof userActivity[index] != 'undefined') {
                userActivity[index] = payload;
                commit("SET_USER_ACTIVITY", userActivity);
            }
        },
        deleteActivity: ({ commit, getters }, index) => {
            let { userActivity } = getters
            //check index available in stored user activity
            if (typeof userActivity[index] != 'undefined') {
                userActivity.splice(index, 1)
                commit("SET_USER_ACTIVITY", userActivity);
            }
        }
    },
    mutations: {
        ["SET_USER_ACTIVITY"](state, payload) {
            state.userActivity = payload;
            storeLocalStorage("userActivity", payload)
        }
    },
});
